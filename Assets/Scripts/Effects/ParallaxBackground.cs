﻿using UnityEngine;

public class ParallaxBackground : MonoBehaviour
{
    [SerializeField] private Vector2 _parallaxEffectMultiplier;
    [SerializeField] private bool _infiniteHorizontal;
    [SerializeField] private bool _infiniteVertical;

    private Transform _camera;

    private Vector3 _lastCameraPosition;

    private float _textureUnitSizeX;
    private float _textureUnitSizeY;

    private void Start()
    {
        if (!(Camera.main is null)) _camera = Camera.main.transform;

        _lastCameraPosition = _camera.position;

        var sprite = GetComponent<SpriteRenderer>().sprite;
        var texture = sprite.texture;

        _textureUnitSizeX = texture.width / sprite.pixelsPerUnit;
        _textureUnitSizeY = texture.height / sprite.pixelsPerUnit;
    }

    private void LateUpdate()
    {
        var deltaMovement = _camera.position - _lastCameraPosition;

        var x = deltaMovement.x * _parallaxEffectMultiplier.x;
        var y = deltaMovement.y * _parallaxEffectMultiplier.y;
        var z = transform.position.z;

        transform.position += new Vector3(x, y, z);

        _lastCameraPosition = _camera.position;

        if (_infiniteHorizontal)
        {
            if (Mathf.Abs(_camera.position.x - transform.position.x) >= _textureUnitSizeX)
            {
                var offsetPositionX = (_camera.position.x - transform.position.x) % _textureUnitSizeX;
                transform.position = new Vector3(_camera.position.x + offsetPositionX , transform.position.y);
            }
        }
        
        if (_infiniteVertical)
        {
            if (Mathf.Abs(_camera.position.y - transform.position.y) >= _textureUnitSizeY)
            {
                var offsetPositionY = (_camera.position.y - transform.position.y) % _textureUnitSizeY;
                transform.position = new Vector3(transform.position.x,  _camera.position.y + offsetPositionY);
            }
        }
    }
}