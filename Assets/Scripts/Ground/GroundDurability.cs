﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public class GroundDurability : ADistructableTile, IDamageble
{
    

    [SerializeField] private SOGround _soGround;
    private float _health;
    private float _distanceBetweenDamageSprites;

    private Collider2D _collider;

    private SpriteRenderer _spriteRenderer;

    void Start()
    {

        _health = _soGround.StartHealth;
        _collider = GetComponent<Collider2D>();

        _spriteRenderer = GetComponent<SpriteRenderer>();
        _distanceBetweenDamageSprites = _health / _soGround.DamagedSprites.Length;
        ChangeSprite();
    }


    private void ChangeSprite()
    {
        _spriteRenderer.sprite = _soGround.DamagedSprites[_soGround.DamagedSprites.Length - (int)Mathf.Ceil(_health / _distanceBetweenDamageSprites)];
    }

    public void Damage(float damageCount)
    {
        _health -= damageCount;
        if (_health <= 0)
        {
            base.DestroyTile();
        }
        else
        {
            ChangeSprite();
        }
    }
}
