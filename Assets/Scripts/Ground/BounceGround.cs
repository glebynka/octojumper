using UnityEngine;

namespace Ground
{
	public class BounceGround : MonoBehaviour
	{
		private void OnCollisionEnter2D(Collision2D collision)
		{
			if (!collision.collider.tag.Equals("Player"))
			{
				return;
			}
			
			var player = collision.gameObject.GetComponent<PlayerBehaviour>();
			if (player)
			{
				player.Animator.SetTrigger("Bounce");
			}
		}
	}
}