﻿using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEngine.Tilemaps;

public class LevelEditorWindow : EditorWindow
{
    public string levelName = "Level";
    public string levelBiom = "Biom_1";

    private const string LevelBiomPath = "Assets/Prefabs/Level";
    private const string LevelTerminalPath = ".asset";
    private const string LevelConfigPath = "Assets/Resources/SOAssets/LevelConfigs/";
    private const string LevelOrderPath = "Assets/Resources/SOAssets/LevelConfigs/LevelOrder.asset";

    [MenuItem("TileMap/Find SOLevelOrder")]
    public static void ExampleScript()
    {
        var window = GetWindow<LevelEditorWindow>("Level Editor");
        window.minSize = new Vector2(300, 450);
        window.Show();
    }

    private void OnGUI()
    {
        GUILayout.Label("_Create_Level_", EditorStyles.boldLabel);

        GUILayout.Space(40);

        levelName = GUI.TextArea(new Rect(10, 30, 200, 20), levelName, 200);

        levelBiom = GUI.TextArea(new Rect(10, 60, 200, 20), levelBiom, 200);

        GUILayout.Space(40);

        if (GUILayout.Button($"Create_Level: {levelName}"))
        {
            var path = LevelConfigPath + levelName + LevelTerminalPath;

            var loadLevelConfig =
                (SOLevelConfig) AssetDatabase.LoadAssetAtPath($"{LevelConfigPath}{levelName}.asset",
                    typeof(SOLevelConfig));

            var levelConfig = loadLevelConfig == null ? CreateInstance<SOLevelConfig>() : loadLevelConfig;

            levelConfig.name = levelName;

            if (loadLevelConfig == null)
            {
                AssetDatabase.CreateAsset(levelConfig, path);
            }

            if (!AssetDatabase.IsValidFolder($"{LevelBiomPath}/{levelBiom}"))
            {
                AssetDatabase.CreateFolder(LevelBiomPath, levelBiom);
            }

            if (!AssetDatabase.IsValidFolder($"{LevelBiomPath}/{levelBiom}/{levelName}"))
            {
                AssetDatabase.CreateFolder($"{LevelBiomPath}/{levelBiom}", levelName);
            }

            GameObject[] objectArray = Selection.gameObjects;

            foreach (var gameObject in objectArray)
            {
                var levelInfoGameObject = gameObject.GetComponent<LevelInfo>();

                switch (levelInfoGameObject.ChunkType)
                {
                    case ChunkType.Start:
                        levelConfig.StartChunk = CreatePrefabChunk(gameObject, "start").GetComponent<Tilemap>();
                        break;
                    case ChunkType.Gameplay:
                        levelConfig.RandomBasisChunk =
                            CreatePrefabChunk(gameObject, "gameplay").GetComponent<Tilemap>();
                        break;
                    case ChunkType.End:
                        levelConfig.EndChunk = CreatePrefabChunk(gameObject, "end").GetComponent<Tilemap>();
                        break;
                }
            }

            var levelOrder = (SOLevelOrder) AssetDatabase.LoadAssetAtPath(LevelOrderPath, typeof(SOLevelOrder));

            if (levelOrder != null)
            {
                levelConfig = (SOLevelConfig) AssetDatabase.LoadAssetAtPath($"{LevelConfigPath}{levelName}.asset",
                    typeof(SOLevelConfig));

                if (levelConfig != null)
                {
                    if (!levelOrder.LevelConfigs.Exists(levelConf => levelConf.name == levelConfig.name))
                    {
                        levelOrder.LevelConfigs.Add(levelConfig);
                    }
                }
            }
        }

        GUILayout.Space(10);

        if (GUILayout.Button($"Delete_Level: {levelName}"))
        {
            if (AssetDatabase.IsValidFolder($"{LevelBiomPath}/{levelBiom}/{levelName}"))
            {
                AssetDatabase.DeleteAsset($"{LevelBiomPath}/{levelBiom}/{levelName}");
            }

            RemoveConfig(levelName);
        }

        GUILayout.Space(40);

        UpdatingLevels();
    }

    private LevelInfo CreatePrefabChunk(GameObject gameObject, string additionPath)
    {
        var currentPath = $"{LevelBiomPath}/{levelBiom}/{levelName}/{levelName}_{additionPath}.prefab";

        var localPath = AssetDatabase.GenerateUniqueAssetPath(currentPath);

        var levelInfo = AssetDatabase.LoadAssetAtPath(currentPath, typeof(LevelInfo)) as LevelInfo;

        if (levelInfo)
        {
            PrefabUtility.ApplyPrefabInstance(gameObject, InteractionMode.UserAction);

            return levelInfo;
        }

        return PrefabUtility.SaveAsPrefabAsset(gameObject, localPath).GetComponent<LevelInfo>();
    }

    private void UpdatingLevels()
    {
        var levelOrder = (SOLevelOrder) AssetDatabase.LoadAssetAtPath(LevelOrderPath, typeof(SOLevelOrder));

        if (levelOrder)
        {
            foreach (var levelConfig in levelOrder.LevelConfigs)
            {
                if (levelConfig == null)
                {
                    return;
                }

                if (GUILayout.Button($"Load Level to Scene: ({levelConfig.name})"))
                {
                    LoadLevelToScene(levelConfig);
                    
                    var levelIndex = levelOrder.LevelConfigs.FindIndex(config => config.name == levelConfig.name);
                    
                    levelOrder.LevelEditingIndex = levelIndex;
                }
            }
        }
    }

    private void LoadLevelToScene(SOLevelConfig levelConfig)
    {
        var levelPlacer = FindObjectOfType<LevelPlacerEditor>();
        var tileMaps = FindObjectsOfType<TilemapRenderer>().ToList();

        foreach (var tileMap in tileMaps)
        {
            if (PrefabUtility.IsPartOfPrefabInstance(tileMap.transform))
            {
                var prefabInstance = PrefabUtility.GetPrefabInstanceHandle(tileMap.transform);

                DestroyImmediate(prefabInstance);
            }

            DestroyImmediate(tileMap.transform.gameObject, true);
        }

        levelPlacer.LoadLevel(levelConfig);
    }

    private void RemoveConfig(string levelConfigName)
    {
        var levelOrder = (SOLevelOrder) AssetDatabase.LoadAssetAtPath(LevelOrderPath, typeof(SOLevelOrder));

        if (levelOrder != null)
        {
            var levelConfig = levelOrder.LevelConfigs.Find(config => config.name == levelConfigName);

            if (levelConfig)
            {
                var levelConfigPath = AssetDatabase.GetAssetPath(levelConfig);

                AssetDatabase.DeleteAsset(levelConfigPath);

                levelOrder.LevelConfigs.Remove(levelConfig);
            }
        }
    }
}