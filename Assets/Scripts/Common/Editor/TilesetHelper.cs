﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class TilesetHelper : EditorWindow
{
    public static string OriginTilePath = "Assets/Sprites/LevelArt/Biom_1/tileset2000х2000v3.png";

    public static string chunkSimpleTag = "tileset";
    public static string chunkTrapTag = "trap";

    private const string TilesPath = "Assets/Prefabs/Tiles/";
    private const string DamageObjectConfigPath = "Assets/Resources/SOAssets/DamageObjectConfig";

    [MenuItem("TileMap/Tile Helper")]
    public static void ExampleScript()
    {
        var window = GetWindow<TilesetHelper>("Tile Helper");
        window.minSize = new Vector2(420, 150);
        window.Show();
    }

    private void OnGUI()
    {
        OriginTilePath = GUI.TextArea(new Rect(10, 30, 400, 50), OriginTilePath, 500);

        if (GUILayout.Button($"Generate_Tiles"))
        {
            var objectArray =AssetDatabase.LoadAllAssetsAtPath(OriginTilePath).ToList();//Selection.objects.ToList();

            var gameObjectList = new List<GameObject>();

            foreach (var selectedObject in objectArray)
            {
                var currentGameObject = new GameObject {name = selectedObject.name};

                gameObjectList.Add(currentGameObject);
            }

            var listOfSimpleTile = new List<GameObject>();

            var listOfTrapTile = new List<GameObject>();

            foreach (var currentObject in gameObjectList)
            {
                if (currentObject.name.Contains(chunkTrapTag))
                {
                    listOfTrapTile.Add(currentObject);
                }

                if (!currentObject.name.Contains(chunkTrapTag))
                {
                    listOfSimpleTile.Add(currentObject);
                }
            }


            foreach (var trapTile in listOfTrapTile)
            {
                CreatePrefabForDangerTile(trapTile);
            }

            foreach (var simpleTile in listOfSimpleTile)
            {
                CreatePrefabForSimpleTile(simpleTile);
            }

            foreach (var tile in gameObjectList)
            {
                DestroyImmediate(tile, true);
            }
        }
    }

    private static void CreatePrefabForDangerTile(GameObject currentObject)
    {
        var currentPath = $"{TilesPath}/TileObjects/{currentObject.name}.prefab";

        var localPath = AssetDatabase.GenerateUniqueAssetPath(currentPath);

        var neededObject = AssetDatabase.LoadAssetAtPath(currentPath, typeof(GameObject)) as GameObject;

        if (neededObject == null)
        {
            neededObject = currentObject;

            AddComponentForTile(neededObject, typeof(BoxCollider2D));
            AddComponentForTile(neededObject, typeof(TilePosition));
            AddComponentForTile(neededObject, typeof(DangerGround));
            AddComponentForTile(neededObject, typeof(SpriteRenderer));

            PrefabUtility.SaveAsPrefabAsset(neededObject, localPath);
        }

        CreateRuleTile(neededObject, localPath, $"{TilesPath}/RuleTile/Traps/{neededObject.name}.asset");
    }

    private static void CreatePrefabForSimpleTile(GameObject currentObject)
    {
        var currentPath = $"{TilesPath}/TileObjects/{currentObject.name}.prefab";

        var localPath = AssetDatabase.GenerateUniqueAssetPath(currentPath);

        var neededObject = AssetDatabase.LoadAssetAtPath(currentPath, typeof(GameObject)) as GameObject;

        if (neededObject == null)
        {
            neededObject = currentObject;

            AddComponentForTile(neededObject, typeof(BoxCollider2D));
            AddComponentForTile(neededObject, typeof(TilePosition));
            AddComponentForTile(neededObject, typeof(SpriteRenderer));

            PrefabUtility.SaveAsPrefabAsset(neededObject, localPath);
        }

        CreateRuleTile(neededObject, localPath, $"{TilesPath}/RuleTile/Simple/{neededObject.name}.asset");
    }

    private static void AddComponentForTile(GameObject gameObject, Type currentType)
    {
        var neededComponent = gameObject.GetComponent(currentType.Name);

        if (neededComponent == null)
        {
            gameObject.AddComponent(currentType);
        }

        if (currentType == typeof(DangerGround))
        {
            var soDanger =
                (SODanger) AssetDatabase.LoadAssetAtPath($"{DamageObjectConfigPath}.asset", typeof(SODanger));

            if (soDanger)
            {
                gameObject.GetComponent<DangerGround>().SoDanger = soDanger;
            }
        }
    }

    private static void CreateRuleTile(GameObject currentGameObject, string prefabPath ,string ruleTilePath)
    {
        var currentPath = ruleTilePath;
        
       // var currentPath = $"{TilesPath}/RuleTile/{currentGameObject.name}.asset";

        var loadRuleTile = (RuleTile) AssetDatabase.LoadAssetAtPath(currentPath, typeof(RuleTile));

        var tileRuleAsset = loadRuleTile == null ? CreateInstance<RuleTile>() : loadRuleTile;

        tileRuleAsset.name = currentGameObject.name;

        var spriteObjects = AssetDatabase.LoadAllAssetsAtPath(OriginTilePath)
            .ToList()
            .Where(currentSprites => currentSprites is Sprite)
            .Cast<Sprite>()
            .ToList()
            .FindAll(sprite => sprite.name == currentGameObject.name);

        var neededSprites = spriteObjects.Find(sprite => sprite.name == currentGameObject.name);

        if (neededSprites)
        {
            tileRuleAsset.m_DefaultSprite = neededSprites;
        }

        var prefab = AssetDatabase.LoadAssetAtPath(prefabPath, typeof(GameObject)) as GameObject;

        tileRuleAsset.Prefab = prefab;


        var tileRule = new RuleTile.TilingRule()
        {
            m_ColliderType = Tile.ColliderType.Sprite,
            m_Output = RuleTile.TilingRule.OutputSprite.Single,
            m_RuleTransform = RuleTile.TilingRule.Transform.Fixed,
            m_Sprites = new[] {neededSprites}
        };

        tileRuleAsset.m_TilingRules.Add(tileRule);

        AssetDatabase.CreateAsset(tileRuleAsset, currentPath);
    }
}