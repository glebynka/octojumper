﻿using System;
using System.Collections.Generic;
using System.IO;
using Newtonsoft.Json;
using UnityEngine;

public class GameSaver
{
    #region Nested Container GameConfig

    [Serializable]
    public class GameConfig
    {
        [Serializable]
        public class LevelInfo
        {
            public string Name = "";
            public string PlayerNameUnLocked = "";
            public bool IsLevelLock = false;
        }

        [Serializable]
        public class SoundInfo
        {
            public float MusicValue = 1.0f;
            public float SFXValue = 1.0f;

            public SoundInfo()
            {
                MusicValue = 1.0f;
                SFXValue = 1.0f;
            }
        }

        public List<LevelInfo> LevelInfoData = new List<LevelInfo>();
        public SoundInfo SoundInfoData = new SoundInfo();
        public int CurrentLevelIndex = 0;
    }

    #endregion

    private const string TerminalFile = "GameData.json";
    private const string DefaultSettings = "Assets/Resources/DefaultSettings/DefaultGameSettings.json";

    private static string _path;

    private static string GetPath
    {
        get
        {
            if (String.IsNullOrEmpty(_path))
            {
                _path = Path.Combine(Application.persistentDataPath, TerminalFile);

                Debug.Log($"_path: {_path}");
            }

            return _path;
        }
    }

    public static void SaveFileJSON()
    {
        SaveFileJSON(LoadingFileJSON<GameConfig>());
    }

    public static void SaveFileJSON(object obj)
    {
        var content = JsonConvert.SerializeObject(obj);

        File.WriteAllText(GetPath, content);
    }

    public static T LoadingFileJSON<T>()
    {
        if (!File.Exists(GetPath))
        {
            SaveFileJSON(new GameConfig());
        }

        return JsonUtility.FromJson<T>(File.ReadAllText(GetPath));
    }

    public static GameConfig LoadDefaultSettings()
    {
        return JsonUtility.FromJson<GameConfig>(File.ReadAllText(DefaultSettings));
    }

    public static void RemoveFileJSON()
    {
        if (File.Exists(GetPath))
        {
            File.Delete(_path);
        }
    }
}