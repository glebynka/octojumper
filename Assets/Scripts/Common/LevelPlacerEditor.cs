﻿#if UNITY_EDITOR

using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LevelPlacerEditor : MonoBehaviour
{
    [SerializeField] private SOLevelOrder _levelOrder;
    private static int _levelCount;

    private SOLevelConfig _levelConfig;
    private static Vector2 _startPosition;

    private static Tilemap _chunkBasis;
    private static Tilemap _chunkStart;
    private static Tilemap _chunkEnd;


    public void LoadLevel(SOLevelConfig levelConfig)
    {
        float posY;
        float posX;

        _chunkBasis = levelConfig.RandomBasisChunk;
        _chunkStart = levelConfig.StartChunk;
        _chunkEnd = levelConfig.EndChunk;

        posY = 0;
        posX = -_chunkBasis.cellBounds.center.x;

        LoadPrefab(posX, posY, _chunkBasis.gameObject);

        posY = _chunkBasis.cellBounds.center.y + _chunkBasis.size.y * 0.5f + _chunkStart.size.y * 0.5f -
               _chunkStart.cellBounds.center.y;
        
        LoadPrefab(posX, posY, _chunkStart.gameObject);

        posY = _chunkBasis.cellBounds.center.y - _chunkBasis.size.y * 0.5f - _chunkEnd.size.y * 0.5f -
               _chunkEnd.cellBounds.center.y;
        
        LoadPrefab(posX, posY, _chunkEnd.gameObject);
    }

    private void LoadPrefab(float posX, float posY , GameObject chunk)
    {
        var chunkBasis = PrefabUtility.InstantiatePrefab(chunk) as GameObject;
        chunkBasis.transform.position = new Vector2(posX, posY);
        chunkBasis.transform.rotation = Quaternion.identity;
        chunkBasis.transform.parent = transform;
    }

#endif
}