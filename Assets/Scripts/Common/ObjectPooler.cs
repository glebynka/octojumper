﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{
    [System.Serializable]
    public class Pool
    {
        public string Tag;
        public GameObject Prefab;
        public int Size;
    }

    [SerializeField] 
    private List<Pool> Pools = new List<Pool>();
    
    private Dictionary<string, Queue<GameObject>> poolDicitionary = new Dictionary<string, Queue<GameObject>>();

    private void Start()
    {
        foreach (var pool in Pools)
        {
            Queue<GameObject> objectPool = new Queue<GameObject>();

            for (int i = 0; i < pool.Size; i++)
            {
                var gameObjectsPool = Instantiate(pool.Prefab);
                
                gameObjectsPool.SetActive(false); 
                
                objectPool.Enqueue(gameObjectsPool);
            }
            
            poolDicitionary.Add(pool.Tag , objectPool);
        }
    }

    public GameObject GetPoolObject(string tag , Vector3 position , Quaternion rotation)
    {
        if (!poolDicitionary.ContainsKey(tag))
        {
            return null;
        }

        var poolObject = poolDicitionary[tag].Dequeue();
        
        poolObject.SetActive(true);
        poolObject.transform.position = position;
        poolObject.transform.rotation = rotation;

        var ipooledObject = poolObject.GetComponent<IPooledObject>();

        ipooledObject?.OnObjectSpawn();


        poolDicitionary[tag].Enqueue(poolObject);

        return poolObject;
    }
}