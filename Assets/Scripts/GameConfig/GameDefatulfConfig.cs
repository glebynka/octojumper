﻿using System;
using UnityEngine;

[CreateAssetMenu(fileName = "DefaultConfig" , menuName = "GameSetting/MainConfig")]
public class GameDefatulfConfig : ScriptableObject
{
    [SerializeField] private GameSaver.GameConfig _gameConfig;

    public GameSaver.GameConfig GameConfig => _gameConfig;
}
