﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SandBlock : MonoBehaviour
{
    [SerializeField] private float _damageAmount = 10f;
    [SerializeField] private float _damageTime = 0.1f;

    private IDamageble _damagebleObject;
    private Collider2D _collider;
    private float _distance;
    private bool _destroy;
    


    private void Start()
    {
        _damagebleObject = GetComponent<IDamageble>();

        _collider = GetComponent<Collider2D>();

        _distance = _collider.bounds.size.y * 0.5f + 0.1f;

        _destroy = false;
    }
   

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag != "Player") return;

        //RaycastHit2D hit = Physics2D.Raycast(_collider.bounds.center, Vector2.down, _distance,gameObject.layer);
        RaycastHit2D hit = Physics2D.Raycast(_collider.bounds.center, Vector2.down, _distance,1 << gameObject.layer);
        Debug.DrawLine(_collider.bounds.center, _collider.bounds.center + Vector3.down * _distance,Color.red , 2f);

        if (hit) return;

        if (_destroy) return;

        _destroy = true;
        StartCoroutine(MakeDamage());
    }

    private IEnumerator MakeDamage()
    {
        var wait = new WaitForSeconds(_damageTime);
        while (true)
        {
            yield return wait;
            _damagebleObject.Damage(_damageAmount);
        }
    }

}
