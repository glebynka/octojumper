﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TilePosition : MonoBehaviour
{
    private Vector3Int _tilePositionPosition;

    public Vector3Int Position
    {
        get => _tilePositionPosition;
        set => _tilePositionPosition = value;
    }
}
