﻿using Events;
using UnityEngine;

public class MenusManager : MonoBehaviour
{
    [SerializeField] private GameObject _noneGameplayMenu;
    [SerializeField] private GameObject _gameplayMenu;
    [SerializeField] private GameObject _finisGamePanel;
    
    private void OnEnable()
    {
        Observer.AddEventListener<LoadingAnimationStartedEvent>(SetActiveMenus);
        Observer.AddEventListener<BackToMainMenuEvent>(SetActiveMenus);
    }
    
    private void Start()
    {
        Observer.Publish(new SoundsEvent("MenuMusic" , true));
    }
    
    private void OnDisable()
    {
        Observer.RemoveEventListener<LoadingAnimationStartedEvent>(SetActiveMenus);
        Observer.AddEventListener<BackToMainMenuEvent>(SetActiveMenus);
    }
    
    private void SetActiveMenus(LoadingAnimationStartedEvent e)
    {
        _noneGameplayMenu.SetActive(false);
        _gameplayMenu.SetActive(true);
        
        Observer.Publish(new SoundsEvent("MenuMusic" , false));
        Observer.Publish(new SoundsEvent("LevelMusic" , true));
    }
    
    private void SetActiveMenus(BackToMainMenuEvent e)
    {
        _noneGameplayMenu.SetActive(true);
        _gameplayMenu.SetActive(false);
        _finisGamePanel.SetActive(false);
        
        Observer.Publish(new SoundsEvent("MenuMusic" , true));
        Observer.Publish(new SoundsEvent("LevelMusic" , false));
    }
}