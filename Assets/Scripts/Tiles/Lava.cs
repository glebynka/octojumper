﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lava : MonoBehaviour
{
    private TileObjectController _tileObjectController;
    private float _distance;
    // Start is called before the first frame update
    void Start()
    {
        _tileObjectController = transform.parent.GetComponent<TileObjectController>();


        var collider = GetComponent<Collider2D>();        
        _distance = collider.bounds.size.y * 0.5f + 0.1f;
        StartCoroutine(Liquidity());
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private IEnumerator Liquidity()
    {
        WaitForSeconds delay = new WaitForSeconds(2f);

        while (true)
        {
            yield return delay;
            RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.down, _distance);

            if (!hit.collider)
            {

                var tilePosition = GetComponent<TilePosition>().Position;

                var newPosition = tilePosition;
                newPosition.y--;

                var tile = _tileObjectController.GetTile(tilePosition);

                _tileObjectController.AddTileObject(newPosition, gameObject, tile);
                yield break;
            }
            
        }
    }
}
