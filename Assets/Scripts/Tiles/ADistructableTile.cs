﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.Tilemaps;

public abstract class ADistructableTile : MonoBehaviour
{
    [SerializeField] private UnityEvent OnDeath = new UnityEvent();
    private Tilemap _tilemap;

    private void Awake()
    {
        _tilemap = transform.parent.GetComponent<Tilemap>();
    }

    private IEnumerator Delete()
    {
        yield return new WaitForEndOfFrame();

        var position = GetComponent<TilePosition>().Position;

        Destroy(gameObject);
        _tilemap.SetTile(position, null);
    }

    public  void DestroyTile()
    {
        OnDeath.Invoke();
        StartCoroutine(Delete());
    }
}
