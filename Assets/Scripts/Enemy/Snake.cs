﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : MonoBehaviour
{
    [SerializeField] private float speed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        var HA = Input.GetAxis("Horizontal");
        var VA = Input.GetAxis("Vertical");
        transform.Translate(new Vector2(HA, VA) * speed * Time.deltaTime);
    }
}
