﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GlobeFish : AEnemy
{
	[SerializeField] private float _distance = 0.1f;
	[SerializeField] private LayerMask _mask = 0;
	[SerializeField] private float _speed;

	[SerializeField] private float _travelDistance = 1f;
	[SerializeField] Transform [] _transformsToMove;
	private Queue<Transform> _transformsQueue = new Queue<Transform>();

	private CircleCollider2D _circleCollider;
	private Transform _targetTransform;

	private void Start()
	{
		_circleCollider = GetComponent<CircleCollider2D>();
		foreach (var transform in _transformsToMove)
		{
			_transformsQueue.Enqueue(transform);
		}
		
		_targetTransform = _transformsQueue.Dequeue();
	}

	private void Update()
	{
		Move();
	}

	public void Move()
	{
		DetectDirection();
		var vectorToGo = Vector3.Normalize(_targetTransform.position - transform.position);
		transform.Translate(vectorToGo * _speed * Time.deltaTime);
	}

	private void DetectDirection()
	{
		if (Vector3.Distance(transform.position, _targetTransform.position) < 0.1f)
		{
			ChangeDirection();
		}
	}

	private void ChangeDirection()
	{
		_transformsQueue.Enqueue(_targetTransform);
		_targetTransform = _transformsQueue.Dequeue();
	}

	private void GetLarge()
	{
		_circleCollider.radius = 1.25f;
	}

	private void GetSmall()
	{
		_circleCollider.radius = 0.25f;
	}
}