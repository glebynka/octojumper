﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class InvasionEvent : UnityEvent{ }

public class DangerEnemyTerritory : MonoBehaviour
{
    [SerializeField] private GameObject _prefabEnemy;
    [SerializeField] private int _countEnemy;
    private Collider2D _collider;


    private void Start()
    {
        Destroy(GetComponentInChildren<SpriteRenderer>().gameObject);
        
        _collider = GetComponent<Collider2D>();
        
        for (int i = 0; i < _countEnemy; i++)
            Instantiate(_prefabEnemy, _collider.bounds.center, Quaternion.identity, transform);
    }

    public InvasionEvent InvasionEvent;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Player") return;

        InvasionEvent.Invoke();
    }

    private void OnDrawGizmos()
    {
        if (!Application.isPlaying)
        {
            var collider = GetComponent<Collider2D>();
            DebugExtension.DebugBounds(collider.bounds, Color.red);
            var art = GetComponentInChildren<SpriteRenderer>();
            var artInPrefab = _prefabEnemy.GetComponentInChildren<SpriteRenderer>();
            art.transform.localScale = artInPrefab.transform.localScale;
            art.sprite = artInPrefab.sprite;
            art.transform.position = collider.bounds.center;

        }
    }
}
