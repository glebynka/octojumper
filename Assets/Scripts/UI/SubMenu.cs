﻿using DG.Tweening;
using Events;
using UnityEngine;

public class SubMenu : MonoBehaviour
{
    [SerializeField] private GameObject _levelPanel;
    [SerializeField] private GameObject _optionsPanel;
    [SerializeField] private GameObject _creditsPanel;
   
    [SerializeField] private float _animationDuration = 0.5f;

    private Vector2 _startPosition = Vector2.zero;
    

    private void OnEnable()
    {
        Observer.AddEventListener<LevelsButtonEvent>(SubMenuListenerIEvent);
        Observer.AddEventListener<OptionsButtonEvent>(SubMenuListenerIEvent);
        Observer.AddEventListener<CreaditsButtonEvent>(SubMenuListenerIEvent);
        Observer.AddEventListener<BackToMainMenuEvent>(DeactivatePanels);
    }

    private void Start()
    {
        var levelPanel = _levelPanel.GetComponent<ISubmenu>();
        var optionsPanel = _optionsPanel.GetComponent<ISubmenu>();
        var creditsPanel = _creditsPanel.GetComponent<ISubmenu>();

        levelPanel?.Init();
        optionsPanel?.Init();
        creditsPanel?.Init();
        
        _startPosition = transform.position;
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<LevelsButtonEvent>(SubMenuListenerIEvent);
        Observer.RemoveEventListener<OptionsButtonEvent>(SubMenuListenerIEvent);
        Observer.RemoveEventListener<CreaditsButtonEvent>(SubMenuListenerIEvent);
        Observer.RemoveEventListener<BackToMainMenuEvent>(DeactivatePanels);
    }

    private void SubMenuListenerIEvent(IEvent menusEvent)
    {
        ActivatePanel(menusEvent);

        AnimateToMainPos();
    }

    private void ActivatePanel(IEvent menusEvent)
    {
        switch (menusEvent)
        {
            case LevelsButtonEvent levelButtonEvent:
                _levelPanel.SetActive(true);
                break;
            case OptionsButtonEvent optionsButtonEvent:
                _optionsPanel.SetActive(true);
                break;
            case CreaditsButtonEvent creaditsButtonEvent:
                _creditsPanel.SetActive(true);
                break;
        }
    }

    private void DeactivatePanels(BackToMainMenuEvent backToMainMenuEvent)
    {
        AnimateToSubPos();
    }

    private void AnimateToMainPos()
    {
        transform.DOMoveY(0, _animationDuration);
    }

    private void AnimateToSubPos()
    {
        transform.DOMoveY(_startPosition.y, _animationDuration).OnComplete(() =>
        {
            _levelPanel.SetActive(false);
            _optionsPanel.SetActive(false);
            _creditsPanel.SetActive(false);
        });
    }
}