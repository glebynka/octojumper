﻿using System;
using Events;
using UnityEngine;
using UnityEngine.UI;

public class InputPanel : MonoBehaviour
{
    [SerializeField] private Button _leftButton;
    [SerializeField] private Button _rightButton;

    private void OnEnable()
    {
       Observer.AddEventListener<GamePauseEvent>(SetEnableButtons);
       Observer.AddEventListener<BackToMainMenuEvent>(SetEnableButtons);
    }
    
    private void OnDisable()
    {
        Observer.RemoveEventListener<GamePauseEvent>(SetEnableButtons);
        Observer.RemoveEventListener<BackToMainMenuEvent>(SetEnableButtons);
    }
    
    private void SetEnableButtons(GamePauseEvent e)
    {
        _leftButton.gameObject.SetActive(!e._isPause);
        _rightButton.gameObject.SetActive(!e._isPause);
    }
    
    private void SetEnableButtons(BackToMainMenuEvent e)
    {
        _leftButton.gameObject.SetActive(true);
        _rightButton.gameObject.SetActive(true);
    }
}