﻿using Events;
using UnityEngine;

public class BackButton : MonoBehaviour
{
    public void Click()
    {
        Observer.Publish(new BackToMainMenuEvent());
        Observer.Publish(new SoundsEvent("ButtonTap" , true));
    }
}
