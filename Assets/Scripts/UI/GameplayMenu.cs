﻿using DG.Tweening;
using Events;
using UnityEngine;
using UnityEngine.UI;

public class GameplayMenu : MonoBehaviour
{
    [SerializeField] private Button _pauseButton;
    [SerializeField] private GameObject _pausePanel;
    [SerializeField] private float _startPositionY = -10;
    
    public void SetPause()
    {
        _pauseButton.transform.DOScale(Vector3.zero, 0.25f);
        _pausePanel.transform.DOMoveY(0, 0.25f);
        
        Observer.Publish(new GamePauseEvent(true));
        Observer.Publish(new SoundsEvent("ButtonTap" , true));
    }
    
    public void SetUnPause()
    {
        Observer.Publish(new SoundsEvent("ButtonTap" , true));
        
        _pauseButton.transform.DOScale(Vector3.one, 0.25f);
        _pausePanel.transform.DOMoveY(_startPositionY, 0.25f).OnComplete(() =>
        {
            Observer.Publish(new GamePauseEvent(false));
        });
    }
    
    public void BackToMainMenu()
    {
        Observer.Publish(new BackToMainMenuEvent());
        Observer.Publish(new SoundsEvent("ButtonTap" , true));
        
        _pausePanel.transform.DOMoveY(_startPositionY, 0.25f).OnComplete(() =>
        {
            _pauseButton.transform.localScale = Vector3.one;
        });
    }
}