﻿using DG.Tweening;
using Events;
using TMPro;
using UnityEngine;

public class LoadingPage : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _loadingNumber;
    
    [SerializeField] private float _animationDuration = 0.5f;

    private Vector2 _startPosition = Vector2.zero;
    
    private void OnEnable()
    {
        Observer.AddEventListener<StartButtonEvent>(SetLoadNumberListener);
        Observer.AddEventListener<LevelButtonEvent>(SetLoadNumberListener);
        Observer.AddEventListener<LevelCompleteEvent>(SetLoadNumberListener);
    }

    private void Start()
    {
        _startPosition = transform.position;
    }
    
    private void OnDisable()
    {
        Observer.RemoveEventListener<StartButtonEvent>(SetLoadNumberListener);
        Observer.RemoveEventListener<LevelButtonEvent>(SetLoadNumberListener);
        Observer.RemoveEventListener<LevelCompleteEvent>(SetLoadNumberListener);
    }

    private void SetLoadNumberListener(IEvent gameEndEvent)
    {
        AutoActivateLoadingPage();
    }
    
    private void SetLoadNumberListener(LevelButtonEvent gameEndEvent)
    {
        ActivateLoadingPage(gameEndEvent.LevelNumber);
    }
    
    private void AutoActivateLoadingPage()
    {
        var gameConfig = GameSaver.LoadingFileJSON<GameSaver.GameConfig>();

        ActivateLoadingPage(gameConfig.CurrentLevelIndex);
    }
    
    
    private void ActivateLoadingPage(int level)
    {
        if (_loadingNumber)
        {
            _loadingNumber.text = $"0{level}";
        }

        AnimateLoadingPage();
    }
    
    private void AnimateLoadingPage()
    {
        transform.DOMoveY(0, _animationDuration)
            .OnComplete(() =>
            {
                Observer.Publish(new LoadingAnimationStartedEvent());
                
                transform.DOMoveY(_startPosition.y, _animationDuration).OnComplete(() =>
                {
                    Observer.Publish(new LoadingAnimationFinishedEvent());
                });
            });
    }
}
