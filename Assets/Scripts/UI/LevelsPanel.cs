﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelsPanel : MonoBehaviour
{
    [SerializeField] private List<Button> _levelsButtons = new List<Button>();

    private void OnEnable()
    {
        UpdateLevelPanel();
    }

    private void UpdateLevelPanel()
    {
        var gameConfig = GameSaver.LoadingFileJSON<GameSaver.GameConfig>();

        if (gameConfig != null)
        {
            if (_levelsButtons.Count != gameConfig.LevelInfoData.Count)
            {
                Debug.Log($"_levelsButtons.Count != gameConfig.LevelInfoData.Count");
                return;
            }
            
            for (int i = 0; i < gameConfig.LevelInfoData.Count; i++)
            {
                _levelsButtons[i].interactable = !gameConfig.LevelInfoData[i].IsLevelLock;
            }
        }
    }
}