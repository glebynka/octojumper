﻿using Events;
using UnityEngine;
using UnityEngine.UI;

public class StartMenu : MonoBehaviour
{
   [SerializeField] private Button _startButton;
   [SerializeField] private Button _levelsButton;
   [SerializeField] private Button _optionsButton;
   [SerializeField] private Button _creaditsButton;

   private void OnEnable()
   {
      _startButton.onClick.AddListener(() =>
      {
         Observer.Publish(new StartButtonEvent());
         Observer.Publish(new SoundsEvent("ButtonTap" , true));
      });
      _levelsButton.onClick.AddListener(() =>
      {
         Observer.Publish(new LevelsButtonEvent());
         Observer.Publish(new SoundsEvent("ButtonTap" , true));
      });
      _optionsButton.onClick.AddListener(() =>
      {
         Observer.Publish(new OptionsButtonEvent());
         Observer.Publish(new SoundsEvent("ButtonTap" , true));
      });
      _creaditsButton.onClick.AddListener(() =>
      {
         Observer.Publish(new CreaditsButtonEvent());
         Observer.Publish(new SoundsEvent("ButtonTap" , true));
      });
   }
}
