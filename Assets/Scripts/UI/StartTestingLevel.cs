﻿using Events;
using UnityEngine;
using UnityEngine.UI;

public class StartTestingLevel : MonoBehaviour
{
   [SerializeField] private Button _startTestingButton;
   [SerializeField] private SOLevelOrder _levelOrder;
   
   private void OnEnable()
   {
      _startTestingButton.onClick.AddListener(() =>
      {
         Observer.Publish(new LevelButtonEvent(_levelOrder.LevelEditingIndex));
         Observer.Publish(new SoundsEvent("ButtonTap" , true));
      });
   }
}
