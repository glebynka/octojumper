﻿using Events;
using UnityEngine;

public class LevelButton : MonoBehaviour
{
    public void SelectLevel(int levelNumber)
    {
        var gameConfig = GameSaver.LoadingFileJSON<GameSaver.GameConfig>();
        
        gameConfig.CurrentLevelIndex = levelNumber;
        
        GameSaver.SaveFileJSON(gameConfig);
        
        Observer.Publish(new LevelButtonEvent(levelNumber));
        Observer.Publish(new SoundsEvent("ButtonTap" , true));
    }
}