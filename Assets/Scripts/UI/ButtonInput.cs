﻿using Events;
using UnityEngine;
using UnityEngine.EventSystems;


public class ButtonInput : MonoBehaviour , IPointerDownHandler
{
    [SerializeField] private int _direction = 1;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        Observer.Publish(new InputButtonEvent(_direction));
    }
}