﻿using DG.Tweening;
using Events;
using UnityEngine;

public class TutorialArrow : MonoBehaviour
{
    [SerializeField] private float _maxScale = 0;
    [SerializeField] private float _duration = 0.1f;
    [SerializeField] private int _arrowDirection = 1;

    private void OnEnable()
    {
        Observer.AddEventListener<InputButtonEvent>(DoScale);
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<InputButtonEvent>(DoScale);
    }

    private void DoScale(InputButtonEvent e)
    {
        if (_arrowDirection != e.DirectionSign)
        {
            return;
        }
        
        transform.DOScale(Vector3.one * _maxScale, _duration).OnComplete(() =>
        {
            transform.DOScale(Vector3.one, _duration);
        });
    }
}
