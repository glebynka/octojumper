﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class OptionsPanel : MonoBehaviour, ISubmenu
{
    [SerializeField] private AudioMixer _audioMixer;
    [SerializeField] private Slider _musicSlider;
    [SerializeField] private Slider _vfxSlider;

    private GameSaver.GameConfig _gameConfig;


    private void OnEnable()
    {
        UpdateOptionPanel();
    }

    public void MusicVolumeValue(float value)
    {
        _audioMixer.SetFloat("MusicVolume", -80 * (1 - value));

        _gameConfig.SoundInfoData.MusicValue = value;

        GameSaver.SaveFileJSON(_gameConfig);
    }

    public void SFXVolumeValue(float value)
    {
        _audioMixer.SetFloat("SFXVolume", -80 * (1 - value));

        _gameConfig.SoundInfoData.SFXValue = value;

        GameSaver.SaveFileJSON(_gameConfig);
    }

    public void Init()
    {
        UpdateOptionPanel();
    }

    private void UpdateOptionPanel()
    {
        _gameConfig = GameSaver.LoadingFileJSON<GameSaver.GameConfig>();

        if (_gameConfig != null)
        {
            _musicSlider.value = _gameConfig.SoundInfoData.MusicValue;
            _vfxSlider.value = _gameConfig.SoundInfoData.SFXValue;

            _musicSlider.onValueChanged.Invoke(_musicSlider.value);
            _vfxSlider.onValueChanged.Invoke(_vfxSlider.value);
        }
    }
}