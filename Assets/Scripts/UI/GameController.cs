﻿using UnityEngine;

public class GameController : MonoBehaviour
{
    [SerializeField] private GameObject _finishGameUI;

    private void Awake()
    {
        _finishGameUI.SetActive(false);
    }

    private void OnEnable()
    {
        Observer.AddEventListener<EndGameEvent>(NextLevel);
        Observer.AddEventListener<PlayerDeadEvent>(GameOver);
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<EndGameEvent>(NextLevel);
        Observer.RemoveEventListener<PlayerDeadEvent>(GameOver);
    }

    private void GameOver(PlayerDeadEvent playerDeadEvent)
    {
        Debug.Log($"On Death");
    }

    private void NextLevel(EndGameEvent endGameEvent)
    {
        var gameConfig = GameSaver.LoadingFileJSON<GameSaver.GameConfig>();

        if (gameConfig.CurrentLevelIndex >= LevelPlacer.levelCount - 1)
        {
            FinishGame();
        }
        else
        {
            gameConfig.CurrentLevelIndex += 1;
            gameConfig.LevelInfoData[gameConfig.CurrentLevelIndex].IsLevelLock = false;

            GameSaver.SaveFileJSON(gameConfig);
            Observer.Publish(new LevelCompleteEvent());
        }
    }

    private void FinishGame()
    {
        _finishGameUI.SetActive(true);
        // Observer.Publish(new LevelCompleteEvent());
    }
}