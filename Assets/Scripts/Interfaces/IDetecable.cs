﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IDetecable 
{
   void DetectionDirection (PlayerBehaviour playerBehaviour);
}
