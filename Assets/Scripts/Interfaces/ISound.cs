﻿
public interface ISound 
{
   bool IsPlay { get; set; }
   string SoundName { get; set; }
}
