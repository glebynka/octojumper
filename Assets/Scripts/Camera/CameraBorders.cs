﻿using UnityEngine;

public class CameraBorders : MonoBehaviour
{
    [SerializeField] private PolygonCollider2D _collider;

    private void OnEnable()
    {
        Observer.AddEventListener<LevelBulidedEvent>(SetCameraBorders);
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<LevelBulidedEvent>(SetCameraBorders);
    }

    private void SetCameraBorders(IEvent loadingAnimationStartedEvent)
    {
        Vector2[] points = new Vector2[4];

        points[0].x = (LevelPlacer.width) * 0.5f;
        points[0].y = LevelPlacer.top;

        points[1].x = (LevelPlacer.width) * -0.5f;
        points[1].y = LevelPlacer.top;

        points[2].x = (LevelPlacer.width) * -0.5f;
        points[2].y = LevelPlacer.bottom;

        points[3].x = (LevelPlacer.width) * 0.5f;
        points[3].y = LevelPlacer.bottom;

        _collider.points = points;
    }
}