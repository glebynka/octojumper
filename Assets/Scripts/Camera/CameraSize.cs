﻿using System;
using Cinemachine;
using UnityEngine;

public class CameraSize : MonoBehaviour
{
    [SerializeField] private float _offsetOfLevelBounds = 0.1f;

    private void OnEnable()
    {
        Observer.AddEventListener<LevelBulidedEvent>(SetCameraSize);
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<LevelBulidedEvent>(SetCameraSize);
    }

    private void Start()
    {
      //  SetCameraSize();
    }

    private void SetCameraSize(LevelBulidedEvent levelBulidedEvent)
    {
        GetComponent<CinemachineVirtualCamera>().m_Lens.OrthographicSize =
            (LevelPlacer.width - _offsetOfLevelBounds) / (Camera.main.aspect * 2);
    }
}
