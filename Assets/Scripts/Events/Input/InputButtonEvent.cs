﻿using UnityEngine;

namespace Events
{
    public class InputButtonEvent: IEvent
    {
        public int DirectionSign;

        public InputButtonEvent(int directionSign)
        {
            DirectionSign = directionSign;
        }
    }
}