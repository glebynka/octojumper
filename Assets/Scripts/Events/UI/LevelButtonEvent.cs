﻿namespace Events
{
    public class LevelButtonEvent: IEvent
    {
        public int LevelNumber = 0;

        public LevelButtonEvent(int levelNumber = 0)
        {
            LevelNumber = levelNumber;
        }
    }
}