﻿public class SoundsEvent : IEvent , ISound
{
    public SoundsEvent(string soundName , bool isPlay)
    {
        SoundName = soundName;
        IsPlay = isPlay;
    }

    public bool IsPlay { get; set; }
    public string SoundName { get; set; }
}