﻿public class GamePauseEvent : IEvent
{
    public bool _isPause;

    public GamePauseEvent(bool isPause)
    {
        _isPause = isPause;
    }
}