﻿using System;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.Serialization;


public class AudioManager : MonoBehaviour
{
    [SerializeField] private AudioConfig _audioConfig;

    private void Awake()
    {
        foreach (var sound in _audioConfig.Sounds)
        {
            sound.AudioSource = gameObject.AddComponent<AudioSource>();
            
            sound.AudioSource.clip = sound.Clip;
            sound.AudioSource.volume = sound.Volume;
            sound.AudioSource.pitch = sound.Pitch;
            sound.AudioSource.loop = sound.Loop;
            sound.AudioSource.outputAudioMixerGroup = sound.AudioMixerGroup;
        }
    }

    private void OnEnable()
    {
        Observer.AddEventListener<SoundsEvent>(PlaySound);
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<SoundsEvent>(PlaySound);
    }

    
    private void PlaySound(ISound soundEvent)
    {
        if (soundEvent.IsPlay)
        {
            PlaySound(soundEvent.SoundName);
        }
        else
        {
            StopSound(soundEvent.SoundName);
        }
    }


    private void PlaySound(string soundName)
    {
        var playableSound = FindSound(soundName);

        playableSound?.AudioSource.Play();
    }

    private void StopSound(string soundName)
    {
        var playableSound = Array.Find(_audioConfig.Sounds, sound => sound.Name == soundName);

        playableSound?.AudioSource.Stop();
    }
    
    private Sound FindSound(string soundName)
    {
        var playableSound = Array.Find(_audioConfig.Sounds, sound => sound.Name == soundName);
        return playableSound;
    }
    
}