﻿using System;
using UnityEngine;
using UnityEngine.Audio;

[Serializable]
public class Sound
{
    public const string AudioMixerGroupNameMaster = "Master";
    public const string AudioMixerGroupNameMusic = "Music";
    public const string AudioMixerGroupNameSFX = "SFX";

    private AudioSource _audioSource;

    public string Name;

    public AudioClip Clip ;

    public AudioMixerGroup AudioMixerGroup;

    public string AudioMixerPath;

    public float Volume ;

    public float Pitch;

    public bool Loop;

    public AudioSource AudioSource
    {
        get => _audioSource;
        set => _audioSource = value;
    }
}
