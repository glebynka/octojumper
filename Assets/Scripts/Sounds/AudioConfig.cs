﻿using UnityEngine;

[CreateAssetMenu(fileName = "NewAudioConfig", menuName = "AudioConfig")]
public class AudioConfig : ScriptableObject
{
    [SerializeField] private Sound[] _sounds;

    public Sound[] Sounds => _sounds;
}
