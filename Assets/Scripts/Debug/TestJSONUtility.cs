﻿using UnityEngine;

public class TestJSONUtility : MonoBehaviour
{

    public void Save()
    {
        GameSaver.SaveFileJSON();
    }

    public void LoadAndSomeChanges()
    {
        var gameConfig = GameSaver.LoadingFileJSON<GameSaver.GameConfig>();
       // gameConfig.LevelInfoData.IsLevelPassed = true;
       // gameConfig.LevelInfoData.PlayerNameUnLocked = "OLOLO";
        
        GameSaver.SaveFileJSON(gameConfig);
    }
    
    public void Delete()
    {
        GameSaver.RemoveFileJSON();
    }
}
