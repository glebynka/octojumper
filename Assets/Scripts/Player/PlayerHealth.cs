﻿using System.Collections;
using UnityEngine;

public class PlayerHealth : MonoBehaviour, IDamageble
{
    [SerializeField] private float _deathTime = 1;
    private PlayerBehaviour _playerBehaviour;
    private float _health;

    private void Start()
    {
        _playerBehaviour = GetComponent<PlayerBehaviour>();
        _health = _playerBehaviour.Config.StartHealth;
    }

    public void Damage(float damageCount)
    {
        _health -= damageCount;

        if (_health <= 0)
        {
            StartCoroutine(DeathCoroutine());
        }
    }

    private IEnumerator DeathCoroutine()
    {
        _playerBehaviour.Animator.SetTrigger("Death");
        _playerBehaviour.isCanMove = false;
        _playerBehaviour.Rb.simulated = false;
        
        Observer.Publish(new SoundsEvent("OctoDeath",true));
        
        yield return new WaitForSeconds(_deathTime);
        Observer.Publish(new PlayerDeadEvent());
    }
}