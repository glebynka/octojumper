﻿using Events;
using UnityEngine;

public class PlayerMoveByKeyboard : MonoBehaviour ,IMovable
{
    [SerializeField] private SOPlayer playerConfig;
    
    private int _direction;
    private bool _isMoving;
    
    private void Update()
    {
        SetDirectionMove(-1 , playerConfig.LeftJump);
        SetDirectionMove(1 , playerConfig.RightJump);
    }

    private void SetDirectionMove(int directionHorizontal , KeyCode activeKey)
    {
        if (Input.GetKeyDown(activeKey))
        {
            Observer.Publish(new InputButtonEvent(directionHorizontal));
            
            _direction = directionHorizontal;

            _isMoving = true;
        }
    }

    public void Move(PlayerBehaviour playerBehaviour)
    {
        if (playerBehaviour != null && playerBehaviour.isCanMove)
        {
            if (_isMoving)
            {
                var jumpVector = (_direction * playerBehaviour.Config.ForceVelocityX * transform.right + transform.up * playerBehaviour.Config.ForceVelocityY) * 
                                 playerBehaviour.Config.ForceJump;

                playerBehaviour.Rb.velocity = jumpVector;

                _isMoving = false;
            }
            
            var velocity = playerBehaviour.Rb.velocity;
            var hoverMultiplier = playerBehaviour.Config.HoverMultiplier;
            var limitVelocityX = playerBehaviour.Config.LimitVelocityX;
            var limitVelocityY = playerBehaviour.Config.LimitVelocityY;

            velocity.x = Mathf.Clamp(velocity.x, -limitVelocityX, limitVelocityX);
            velocity.y = Mathf.Clamp(velocity.y, -limitVelocityY / hoverMultiplier, limitVelocityY);

            playerBehaviour.Rb.velocity = velocity;
        }
    }
}