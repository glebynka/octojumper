﻿using UnityEngine;

[CreateAssetMenu(fileName = " new Player Config", menuName = "Configs/PlayerConfig")]
public class SOPlayer : ScriptableObject
{
    public float StartHealth;

    [Header("KeySettings")]
    [SerializeField]
    private KeyCode _leftJump = KeyCode.LeftArrow; 
    
    [SerializeField]
    private KeyCode _rightJump = KeyCode.RightArrow;
    
    [Header("[Movement Settings]")] 
    [Space(10)] public float ForceJump;
    

    [SerializeField] private float _limitVelocityX = 10.0f;

    [SerializeField] private float _limitVelocityY = 10.0f;

    [SerializeField] private float _forceVelocityY = 2.0f;

    [SerializeField] private float _forceVelocityX = 2.0f;

    [SerializeField] private float _fallMultiplier = 2.0f;

    [SerializeField] private float _hoverMultiplier = 2.0f;

    [SerializeField] private float _octopusRotateX = 5.0f;
    
    [SerializeField] private float _octopusRotateDuration = 0.5f;

    public float LimitVelocityX => _limitVelocityX;

    public float LimitVelocityY => _limitVelocityY;

    public float ForceVelocityY => _forceVelocityY;

    public float ForceVelocityX => _forceVelocityX;

    public float FallMultiplier => _fallMultiplier;

    public float HoverMultiplier => _hoverMultiplier;

    public float OctopusRotateDuration => _octopusRotateDuration;

    public float OctopusRotateX => _octopusRotateX;

    public KeyCode LeftJump => _leftJump;

    public KeyCode RightJump => _rightJump;
}
    
