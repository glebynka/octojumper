﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpCommon : MonoBehaviour
{
    [SerializeField] private SOPlayer _soPlayer;

    private Rigidbody2D _rigidbody2D;

    private void Awake()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (_rigidbody2D.velocity.y < 0)
        {
            _rigidbody2D.velocity += Vector2.up * Physics2D.gravity.y * (_soPlayer.FallMultiplier - 1) * Time.deltaTime;
        }
    }
}