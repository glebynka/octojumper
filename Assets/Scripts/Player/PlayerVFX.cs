﻿using Events;
using UnityEngine;

public class PlayerVFX : MonoBehaviour, IPlayerVFXable
{
    private PlayerBehaviour _playerBehaviour;

    private void OnEnable()
    {
        Observer.AddEventListener<InputButtonEvent>(DoVFX);
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<InputButtonEvent>(DoVFX);
    }

    private void DoVFX(InputButtonEvent e)
    {
        if (_playerBehaviour.isCanMove)
        {
            _playerBehaviour.Animator.SetTrigger("Jump");

            Observer.Publish(new SoundsEvent("Jump", true));
        }
    }


    public void InitPlayer(PlayerBehaviour playerBehaviour)
    {
        _playerBehaviour = playerBehaviour;
    }
}