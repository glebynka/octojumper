﻿using DG.Tweening;
using UnityEngine;

public class PlayerRotation : MonoBehaviour, IRotatable
{
    [SerializeField] private Transform _spriteTransform;
    
    public void PLayerRotate(PlayerBehaviour playerBehaviour)
    {
        var rotateVector = new Vector3(0, 0, -playerBehaviour.Rb.velocity.x * playerBehaviour.Config.OctopusRotateX);

        _spriteTransform.DORotate(rotateVector, playerBehaviour.Config.OctopusRotateDuration);
    }
}