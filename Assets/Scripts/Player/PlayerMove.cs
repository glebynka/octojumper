﻿using Events;
using UnityEngine;

public class PlayerMove : MonoBehaviour, IMovable
{
    private int _direction;
    private bool _isMoving = false;

    private void OnEnable()
    {
        Observer.AddEventListener<InputButtonEvent>(SetDirection);
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<InputButtonEvent>(SetDirection);
    }

    private void SetDirection(InputButtonEvent e)
    {
        _direction = e.DirectionSign;
        _isMoving = true;
    }

    public void Move(PlayerBehaviour playerBehaviour)
    {
        if (playerBehaviour != null && playerBehaviour.isCanMove)
        {
            if (_isMoving)
            {
                var jumpVector = (_direction * playerBehaviour.Config.ForceVelocityX * transform.right + transform.up * playerBehaviour.Config.ForceVelocityY) * 
                                 playerBehaviour.Config.ForceJump;

                playerBehaviour.Rb.velocity = jumpVector;

                _isMoving = false;
            }
            

            var velocity = playerBehaviour.Rb.velocity;
            var hoverMultiplier = playerBehaviour.Config.HoverMultiplier;
            var limitVelocityX = playerBehaviour.Config.LimitVelocityX;
            var limitVelocityY = playerBehaviour.Config.LimitVelocityY;

            velocity.x = Mathf.Clamp(velocity.x, -limitVelocityX, limitVelocityX);
            velocity.y = Mathf.Clamp(velocity.y, -limitVelocityY / hoverMultiplier, limitVelocityY);

            playerBehaviour.Rb.velocity = velocity;
        }
    }
}