﻿using System.Collections;
using UnityEngine;

public class SetPlayerToStartPosition : MonoBehaviour
{
    [SerializeField] private float _startDelay = 1;
    
    private PlayerBehaviour _player;


    private void OnEnable()
    {
       Observer.AddEventListener<PlayerDeadEvent>(Reposition);
       Observer.AddEventListener<LevelBulidedEvent>(Reposition);
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<PlayerDeadEvent>(Reposition);
        Observer.RemoveEventListener<LevelBulidedEvent>(Reposition);
    }

    private void Start()
    {
        _player = GetComponent<PlayerBehaviour>();
    }

    private void Reposition()
    {
        if (_player)
        {
            _player.Rb.velocity = Vector2.zero;
        }
        
        transform.position = LevelPlacer.startPosition;
        
        StartCoroutine(StartMoving());
    }
    
    private void Reposition(IEvent playerEvent)
    {
        Reposition();
    }

    private IEnumerator StartMoving()
    {
        _player.Rb.simulated = true;
        _player.Rb.AddForce(new Vector2(0, 10f), ForceMode2D.Impulse);
        yield return new WaitForSeconds(_startDelay);
        _player.isCanMove = true;
    }
    
}
