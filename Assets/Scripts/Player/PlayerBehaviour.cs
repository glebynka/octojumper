﻿using UnityEngine;

public class PlayerBehaviour : MonoBehaviour
{
    [SerializeField] private SOPlayer PlayerConfig;

    public Collider2D Collider => _collider;
    public Rigidbody2D Rb => _rigidbody;
    public SOPlayer Config => PlayerConfig;
    public Animator Animator => _animator;
    
    public bool isCanMove;

    private Animator _animator;
    private Collider2D _collider;
    private Rigidbody2D _rigidbody;

    private IRayable _rayable;
    private IMovable _movable;
    private IDetecable _detecable;
    private IPlayerVFXable _playerVfXable;
    private IRotatable _rotatable;

    private void Awake()
    {
        _animator = GetComponent<Animator>();

        _collider = GetComponent<Collider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();

        _rayable = GetComponent<IRayable>();
        _movable = GetComponent<IMovable>();
        _detecable = GetComponent<IDetecable>();
        _playerVfXable = GetComponent<IPlayerVFXable>();
        _rotatable = GetComponent<IRotatable>();
        
        _playerVfXable?.InitPlayer(this);
    }

    private void Update()
    {
        _detecable?.DetectionDirection(this);
        _rayable?.Looking(this);
        
        if (isCanMove)
        {
            _movable?.Move(this);
        }
        
        _rotatable?.PLayerRotate(this);
    }
}