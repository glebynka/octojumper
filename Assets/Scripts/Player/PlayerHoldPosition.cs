﻿using UnityEngine;

public class PlayerHoldPosition : MonoBehaviour
{
    [SerializeField] private Rigidbody2D _playerRigitbody;

    private void Awake()
    {
        _playerRigitbody.simulated = false;
    }

    private void OnEnable()
    {
        Observer.AddEventListener<LoadingAnimationStartedEvent>(PlayerActive);
        Observer.AddEventListener<LevelCompleteEvent>(PlayerHold);
        Observer.AddEventListener<GamePauseEvent>(PlayerInPause);
    }

    private void PlayerInPause(GamePauseEvent e)
    {
        if (_playerRigitbody)
        {
            _playerRigitbody.simulated = !e._isPause;
        }
    }

    private void PlayerHold(LevelCompleteEvent e)
    {
        if (_playerRigitbody)
        {
            _playerRigitbody.simulated = false;
        }
    }

    private void PlayerActive(LoadingAnimationStartedEvent e)
    {
        if (_playerRigitbody)
        {
            _playerRigitbody.simulated = true;

           // Observer.Publish(new PlayerActiveEvent());
        }
    }


    private void OnDisable()
    {
        Observer.RemoveEventListener<LevelCompleteEvent>(PlayerHold);
        Observer.RemoveEventListener<LoadingAnimationStartedEvent>(PlayerActive);
        Observer.RemoveEventListener<GamePauseEvent>(PlayerInPause);
    }
}