﻿using UnityEngine;

public class GameInitialization : MonoBehaviour
{
    [SerializeField] private GameDefatulfConfig _gameDefatulfConfig;

    private void Awake()
    {
        SetDefaultLevelsData();
        SetFramerate();

#if UNITY_STANDALONE
        Screen.SetResolution(564, 960, false);
#endif
    }

    private void SetDefaultLevelsData()
    {
        var gameConfig = GameSaver.LoadingFileJSON<GameSaver.GameConfig>();
        
        if (gameConfig != null)
        {
            if (gameConfig.LevelInfoData.Count <= 0)
            {
                gameConfig = _gameDefatulfConfig.GameConfig;
                
                Debug.Log($"Set default settings ");
            }
        }
        
        GameSaver.SaveFileJSON(gameConfig);
    }

    private void SetFramerate()
    {
        Application.targetFrameRate = 60;
    }
}