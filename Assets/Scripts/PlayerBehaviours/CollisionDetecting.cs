﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionDetecting : MonoBehaviour
{
    [SerializeField] private float _contactDistance = 0.1f;
    [SerializeField] private Collider2D _collider;

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.collider.name);      
    }
}
