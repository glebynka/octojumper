﻿using Events;
using UnityEngine;

public class FinishGame : MonoBehaviour
{
	private void OnEnable()
	{
		Observer.Publish(new GamePauseEvent(true));
		Observer.Publish(new SoundsEvent("EndGame" , true));
		Observer.Publish(new SoundsEvent("LevelMusic" , false));
	}

	public void RestartGame()
	{
		
		Observer.Publish(new BackToMainMenuEvent());
		Observer.Publish(new SoundsEvent("ButtonTap" , true));
	}
}
