﻿using UnityEngine;

public class EndLevel : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<PlayerBehaviour>())
        {
            Observer.Publish(new EndGameEvent());
        }
    }
}
