﻿
using UnityEngine;

public class LevelInfo : MonoBehaviour
{
    [SerializeField] private ChunkType _chunkType;

    public ChunkType ChunkType => _chunkType;
}

public enum ChunkType
{
    Start , Gameplay , End
}
