﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[CreateAssetMenu(fileName = "NewLevelOrder", menuName = "LevelOrder")]
public class SOLevelOrder : ScriptableObject
{
    [SerializeField] private List<SOLevelConfig> _levelConfigs;

    public int LevelEditingIndex = 0;
    
    public List<SOLevelConfig> LevelConfigs
    {
        get { return _levelConfigs; }
        set => _levelConfigs = value;
    }

    public SOLevelConfig GetLevelConfig(int number)
    {
        return _levelConfigs[number];
    }

    public int levelCount => _levelConfigs.Count;
}
