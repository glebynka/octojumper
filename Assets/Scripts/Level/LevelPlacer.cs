﻿using System;
using System.Collections;
using System.Collections.Generic;
using Events;
using UnityEngine;
using UnityEngine.Tilemaps;

public class LevelPlacer : MonoBehaviour
{
    [SerializeField] private SOLevelOrder _levelOrder;
    private static int _levelCount;
    
    private SOLevelConfig _levelConfig;
    private static Vector2 _startPosition;
    private static float _top;
    private static float _bottom;
    private static float _width;
    
    private static Tilemap _chunkBasis;
    private static Tilemap _chunkStart;
    private static Tilemap _chunkEnd;

    public static Vector2 startPosition => _startPosition;
    public static float top => _top;
    public static float bottom => _bottom;
    public static float width => _width;
    public static int levelCount => _levelCount;

    private bool _IsListenLoadingPageAnimate = true;
    
   private void Awake()
   {
       _levelCount = _levelOrder.levelCount;
   }

   // TODO WILL BE OK !
    private void OnEnable()
    {
        Observer.AddEventListener<LoadingAnimationStartedEvent>(LoadLevel);
        Observer.AddEventListener<LoadingAnimationFinishedEvent>(ActivateListenerLoadingPage);
        Observer.AddEventListener<LevelButtonEvent>(LoadLevel);
    }

    private void OnDisable()
    {
        Observer.RemoveEventListener<LoadingAnimationStartedEvent>(LoadLevel);
        Observer.RemoveEventListener<LoadingAnimationFinishedEvent>(ActivateListenerLoadingPage);
        Observer.RemoveEventListener<LevelButtonEvent>(LoadLevel);
    }

    private void ActivateListenerLoadingPage(LoadingAnimationFinishedEvent e)
    {
        _IsListenLoadingPageAnimate = true;
    }
    
    private void LoadLevel(LevelButtonEvent e)
    {
        _IsListenLoadingPageAnimate = false;

        LoadLevel(e.LevelNumber);
        
        Observer.Publish(new LevelBulidedEvent());
    }

    private void LoadLevel(LoadingAnimationStartedEvent e)
    {
        if (!_IsListenLoadingPageAnimate)
        {
            return;
        }
        
        Debug.Log($"Load level");
        
        var gameConfig = GameSaver.LoadingFileJSON<GameSaver.GameConfig>();
        var number = gameConfig.CurrentLevelIndex;
        
        LoadLevel(number);
        
        Observer.Publish(new LevelBulidedEvent());
    }



    private void LoadLevel(int levelNumber)
    {
        _levelConfig = _levelOrder.GetLevelConfig(levelNumber);

        foreach (Transform child in gameObject.transform)
        {
            Destroy(child.gameObject);
        }

        float posY;
        float posX;

        _chunkBasis = _levelConfig.RandomBasisChunk;
        _chunkStart = _levelConfig.StartChunk;
        _chunkEnd = _levelConfig.EndChunk;

        posY = 0;
        posX = -_chunkBasis.cellBounds.center.x;

        Instantiate(_chunkBasis.gameObject, new Vector2(posX, posY), Quaternion.identity, gameObject.transform);

        posY = _chunkBasis.cellBounds.center.y + _chunkBasis.size.y * 0.5f + _chunkStart.size.y * 0.5f -
               _chunkStart.cellBounds.center.y;
        Instantiate(_chunkStart.gameObject, new Vector2(posX, posY), Quaternion.identity, gameObject.transform);

        posY = _chunkBasis.cellBounds.center.y - _chunkBasis.size.y * 0.5f - _chunkEnd.size.y * 0.5f -
               _chunkEnd.cellBounds.center.y;
        Instantiate(_chunkEnd.gameObject, new Vector2(posX, posY), Quaternion.identity, gameObject.transform);


        AssignTopPosition();
        AssignBottomPosition();
        AssignWidth();

        AssignStartPosition();
    }

    private void AssignStartPosition()
    {
        _startPosition = new Vector2( 0, _bottom + 2f);
    }

    private void AssignTopPosition()
    {
        _top = _chunkBasis.cellBounds.center.y + _chunkBasis.size.y * 0.5f + _chunkStart.size.y;
    }
    
    private void AssignBottomPosition()
    {
        _bottom = _chunkBasis.cellBounds.center.y - _chunkBasis.size.y * 0.5f - _chunkEnd.size.y;
    }

    private void AssignWidth()
    {
        _width = _chunkBasis.size.x;
    }
    
}
