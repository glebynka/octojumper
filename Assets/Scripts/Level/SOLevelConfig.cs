﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(fileName = "NewLevelConfig", menuName = "LevelConfig")]
public class SOLevelConfig : ScriptableObject
{
    [SerializeField] private Tilemap _startChunk;
    [SerializeField] private Tilemap _endChunk;
    [SerializeField] private List<Tilemap>_chunkToChoose = new List<Tilemap>();

    public Tilemap RandomBasisChunk
    {
        get
        {
            var chunk = _chunkToChoose[Random.Range(0, _chunkToChoose.Count)];
            chunk.CompressBounds();
            return chunk;
        }
        set
        {
            if (_chunkToChoose.Contains(value))
            {
                _chunkToChoose[0] = value;
            }
            else
            {
                _chunkToChoose.Add(value);
            }
            
            _chunkToChoose[0].CompressBounds();
        }
    }

    public Tilemap StartChunk
    {
        get
        {
            _startChunk.CompressBounds();
            return _startChunk;
        }
        set
        {
            _startChunk = value; 
            _startChunk.CompressBounds();
        }
    }

    public Tilemap EndChunk
    {
        get
        {
            _endChunk.CompressBounds();
            return _endChunk;
        }
        set
        {
            _endChunk = value;
            _endChunk.CompressBounds();
        }
    }
}